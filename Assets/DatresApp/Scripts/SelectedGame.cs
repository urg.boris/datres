﻿using UnityEngine;
using UnityEngine.Serialization;

namespace DatresApp.Scripts
{
    [CreateAssetMenu(fileName = "SelectedGame", menuName = "CustomData/SelectedGame", order = 0)]
    public class SelectedGame : ScriptableObject
    {
        public string gameType;
        public string gameId;
    }
}