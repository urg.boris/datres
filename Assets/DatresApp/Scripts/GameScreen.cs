﻿using System;
using System.Globalization;
using DatresApp.Scripts.HumbleObjects;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DatresApp.Scripts
{
    public class GameScreen : MonoBehaviour
    {
        [SerializeField] private Button sentScoreButton;
        [SerializeField] private TMP_Text sentScoreText;
        [SerializeField] private TMP_Text scoreText;
        [SerializeField] private float initScore = 999999;
        [SerializeField] private PersistentDataManager persistentDataManager;
        [SerializeField] private DatresRestClient datresRestClient;
        private float _currentScore;

        private bool _isGameFinished = false;

        private void Start()
        {
            AttachListeners();
        }

        private void Update()
        {
            if (!_isGameFinished)
            {
                _currentScore -= (int) (1000 * Time.deltaTime);
                UpdateCurrentScoreText();
            }
        }

        private void OnEnable()
        {
            SetInitState();
        }

        private void OnDestroy()
        {
            DettachListeners();
        }

        private void SetInitState()
        {
            _currentScore = initScore;
            UpdateCurrentScoreText();
        }

        private void UpdateCurrentScoreText()
        {
            scoreText.text = _currentScore.ToString();
        }

        private void AttachListeners()
        {
            sentScoreButton.onClick.AddListener(OnSentScoreButtonClick);
        }

        private void SaveNewScore()
        {
            int score = int.Parse(scoreText.text);
            sentScoreText.text = "sending...";
            try
            {
                datresRestClient.PostNewScore(
                    new NewScoreData(
                        persistentDataManager.SelectedGame.gameType,
                        persistentDataManager.UserName,
                        persistentDataManager.SelectedGame.gameId,
                        score
                    ),
                    OnNewScoreCreatedCallback);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }

        private void OnNewScoreCreatedCallback(GameData obj)
        {
            sentScoreText.text = "saved";
        }

        private void OnSentScoreButtonClick()
        {
            _isGameFinished = true;
            sentScoreButton.interactable = false;
            SaveNewScore();
        }

        private void DettachListeners()
        {
            sentScoreButton.onClick.RemoveAllListeners();
        }
    }
}