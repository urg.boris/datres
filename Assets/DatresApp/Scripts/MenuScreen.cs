﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DatresApp.Scripts.HumbleObjects;
using Proyecto26;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace DatresApp.Scripts
{
    public class MenuScreen : MonoBehaviour
    {
        [SerializeField] private TMP_Dropdown gamesDropdown;
        [SerializeField] private Button playButton;
        [SerializeField] private ScrollRect scoresScrollRect;
        [SerializeField] private TMP_Text lastGamesText;
        [SerializeField] private ScreenLoader screenLoader;
        [SerializeField] private DatresRestClient datresRestClient;
        [SerializeField] private PersistentDataManager persistentDataManager;

        private void Start()
        {
            AttachListeners();
        }

        private void OnEnable()
        {
            SetInitState();
        }

        private void SetInitState()
        {
            PopulateDropDownWithGames();
            lastGamesText.text = "";
            gamesDropdown.gameObject.SetActive(true);
            gamesDropdown.interactable = true;
            scoresScrollRect.gameObject.SetActive(false);
            playButton.gameObject.SetActive(false);
        }

        private void PopulateDropDownWithGames()
        {
            var options = StringConsts.Games.Select(gameType => new TMP_Dropdown.OptionData(gameType)).ToList();

            gamesDropdown.ClearOptions();
            gamesDropdown.AddOptions(options);
        }

        private void LoadLastGames(int index = 0)
        {
            var lastGame = StringConsts.Games[index];
            persistentDataManager.SelectedGame.gameType = lastGame;
            gamesDropdown.interactable = false;
            try
            {
                datresRestClient.GetLastGames(lastGame, OnLastGamesLoadedCallback);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }

        private void StartNewGame()
        {
            var selectedGame = persistentDataManager.SelectedGame.gameType;
            try
            {
                datresRestClient.PostNewGame(new NewGameData(selectedGame, persistentDataManager.UserName),
                    OnNewGameCreatedCallback);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }


        public void OnLastGamesLoadedCallback(GameData[] gameData)
        {
            lastGamesText.text = JsonHelper.ArrayToJsonString(gameData, true);
            playButton.gameObject.SetActive(true);
        }

        public void OnNewGameCreatedCallback(GameData gameData)
        {
            persistentDataManager.SelectedGame.gameId = gameData.id;
            screenLoader.OpenGameScreen();
        }


        private void OnPlayButtonClick()
        {
            StartNewGame();
        }

        private void AttachListeners()
        {
            playButton.onClick.AddListener(OnPlayButtonClick);
            gamesDropdown.onValueChanged.AddListener(OnGameTypeChanged);
        }

        private void OnGameTypeChanged(int index)
        {
            gamesDropdown.interactable = false;
            scoresScrollRect.gameObject.SetActive(true);
            LoadLastGames(index);
        }

        private void DettachListeners()
        {
            playButton.onClick.RemoveAllListeners();
        }

        private void OnDestroy()
        {
            DettachListeners();
        }
    }
}