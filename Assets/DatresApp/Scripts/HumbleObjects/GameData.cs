﻿using System;

namespace DatresApp.Scripts.HumbleObjects
{
    [Serializable]
    public class GameData
    {
        public int ttl;
        public int score1;
        public int score2;
        public string id;
        public string user1;
        public string user2;
        public string type;
    }
}