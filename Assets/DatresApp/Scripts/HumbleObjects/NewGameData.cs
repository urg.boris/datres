﻿using System;

namespace DatresApp.Scripts.HumbleObjects
{
    [Serializable]
    public class NewGameData
    {
        public string gameType;
        public string userName;

        public NewGameData(string gameType, string userName)
        {
            this.gameType = gameType;
            this.userName = userName;
        }
    }
}