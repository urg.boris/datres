﻿using System;

namespace DatresApp.Scripts.HumbleObjects
{
    [Serializable]
    public class NewScoreData
    {
        public string gameType;
        public string userName;
        public string gameId;
        public int score;

        public NewScoreData(string gameType, string userName, string gameId, int score)
        {
            this.gameType = gameType;
            this.userName = userName;
            this.gameId = gameId;
            this.score = score;
        }
    }
}