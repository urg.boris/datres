﻿using System;
using System.Collections.Generic;
using DatresApp.Scripts.HumbleObjects;
using Proyecto26;
using TMPro;
using UnityEditor;
using UnityEngine;

namespace DatresApp.Scripts
{
    public class DatresRestClient : MonoBehaviour
    {
        public void GetLastGames(string gameType, Action<GameData[]> callback)
        {
            var currentRequest = new RequestHelper
            {
                Uri = StringConsts.Url + StringConsts.GetGamesEndpoint,
                Headers = StringConsts.Headers,
                Params = new Dictionary<string, string>
                {
                    {"gameType", gameType}
                }
            };


            RestClient.GetArray<GameData>(currentRequest).Then(allGames =>
                {
                    callback?.Invoke(allGames);
                   // textField.text = JsonHelper.ArrayToJsonString<GameData>(allGames, true);
                })
                .Catch(err =>
                {
                    var error = err as RequestException;
                    throw new Exception(error.Response);
                });
        }

        public void PostNewGame(NewGameData newGameData, Action<GameData> callback)
        {
            var currentRequest = new RequestHelper
            {
                Uri = StringConsts.Url + StringConsts.NewGameEndPoint,
                Headers = StringConsts.Headers,
                Body = newGameData
            };

            RestClient.Post<GameData>(currentRequest)
                .Then(currentGame =>
                {
                    callback?.Invoke(currentGame);
                }).Catch(err =>
                {
                    var error = err as RequestException;
                    throw new Exception(error.Response);
                });
        }
        
        public void PostNewScore(NewScoreData newScoreData, Action<GameData> callback)
        {
            var currentRequest = new RequestHelper
            {
                Uri = StringConsts.Url + StringConsts.WriteScoresEndPoint,
                Headers = StringConsts.Headers,
                Body = newScoreData
            };

            RestClient.Post<GameData>(currentRequest)
                .Then(currentGame =>
                {
                    callback?.Invoke(currentGame);
                }).Catch(err =>
                {
                    var error = err as RequestException;
                    throw new Exception(error.Response);
                });
        }
    }
}