using JetBrains.Annotations;
using UnityEngine;

namespace DatresApp.Scripts
{
    public class ScreenLoader : MonoBehaviour
    {
        [SerializeField] private GameObject introScreen;
        [SerializeField] private GameObject menuScreen;
        [SerializeField] private GameObject gameScreen;
        [SerializeField] private PersistentDataManager persistentDataManager;
        [SerializeField] private GameObject[] screens;


        private void Start()
        {
            persistentDataManager.ClearSelectedGame();
            CloseAllScreens();
            if (!persistentDataManager.IsUserNameSet())
            {
                OpenIntroScreen();
            }
            else
            {
                OpenMenuScreen();
            }
        }

        private void CloseAllScreens()
        {
            introScreen.SetActive(false);
            menuScreen.SetActive(false);
            gameScreen.SetActive(false);
        }


        private void OpenScreen([CanBeNull] GameObject screenToOpen = null)
        {
            foreach (var screen in screens)
            {
                var shouldBeOpened = screenToOpen != null && screen == screenToOpen;
                screen.SetActive(shouldBeOpened);
            }
        }


        public void OpenIntroScreen()
        {
            OpenScreen(introScreen);
        }

        public void OpenMenuScreen()
        {
            OpenScreen(menuScreen);
        }

        public void OpenGameScreen()
        {
            OpenScreen(gameScreen);
        }
    }
}