﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


namespace DatresApp.Scripts
{
    public class PersistentDataManager : MonoBehaviour
    {
        [SerializeField] private SelectedGame selectedGame;
        public SelectedGame SelectedGame => selectedGame;

        public bool IsUserNameSet() => PlayerPrefs.HasKey(StringConsts.UserNameKey);


        public string UserName
        {
            get => PlayerPrefs.GetString(StringConsts.UserNameKey);
            set => PlayerPrefs.SetString(StringConsts.UserNameKey, value);
        }

        public void ClearSelectedGame()
        {
            selectedGame.gameId = "";
            selectedGame.gameType = "";
        }

#if UNITY_EDITOR
        [MenuItem("Tools/delete username")]
        public static void DeleteUserName()
        {
            PlayerPrefs.DeleteKey(StringConsts.UserNameKey);
        }
#endif
    }
}