﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DatresApp.Scripts
{
    public class IntroScreen : MonoBehaviour
    {
        [SerializeField] private TMP_InputField userNameInput;
        [SerializeField] private Button registerButton;
        [SerializeField] private ScreenLoader screenLoader;
        [SerializeField] private PersistentDataManager persistentDataManager;

        private void Start()
        {
            AttachListeners();
        }

        private void AttachListeners()
        {
            registerButton.onClick.AddListener(OnRegisterButtonClick);
        }

        private void DettachListeners()
        {
            registerButton.onClick.RemoveAllListeners();
        }

        private void OnRegisterButtonClick()
        {
            if (userNameInput.text == "") return;
            
            persistentDataManager.UserName = userNameInput.text;
            screenLoader.OpenMenuScreen();
        }


        private void OnDestroy()
        {
            DettachListeners();
        }
    }
}