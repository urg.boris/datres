﻿using System.Collections.Generic;

namespace DatresApp.Scripts
{
    public static class StringConsts
    {
        public const string UserNameKey = "username";

        public static readonly string[] Games = {"select game", "testGameA", "testGameB", "testGameC"};

        public const string Url = "https://uigo9u4i79.execute-api.eu-west-1.amazonaws.com/default";
        
        public static readonly Dictionary<string, string> Headers = new Dictionary<string, string>
        {
            {"Authorization", "3e44ed94-441d-4f19-817a-de37bfdc2771"}
        };  
        
        public const string GetGamesEndpoint = "/getGames";
        
        public const string NewGameEndPoint = "/newGame";
        
        public const string WriteScoresEndPoint = "/writeScore";
    }
}